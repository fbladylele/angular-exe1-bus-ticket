import { DanhSachGheComponent } from '../danh-sach-ghe/danh-sach-ghe.component';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-edit-dsghe',
  templateUrl: './edit-dsghe.component.html',
  styleUrls: ['./edit-dsghe.component.scss'],
})
export class EditDSGheComponent implements OnInit {
  //28->down
  @ViewChild(DanhSachGheComponent) DSGCom!: DanhSachGheComponent;

  //30: đặt template variable cho h2 @editghe.html và dom nó =viewchild:->down 31
  @ViewChild('title') titleHeading!: ElementRef;

  constructor() {}

  //29: đặt template variable cho 4 input @editghe.html, và (click) cho button lấy value ô input->30 up
  //truyền [tham số] thay vì gọi tên từng tham số
  themGheParent(...thamso: any[]) {
    let gheDuocThem = {
      TenGhe: thamso[0],
      SoGhe: parseInt(thamso[1]),
      Gia: parseInt(thamso[2]),
      TrangThai: thamso[3],
      // TrangThai: Boolean.apply(thamso[3]), -> ép kiểu ghi true nó vẫn trả false zô ziên ko xài, xuống dưới if else để chuyển kdl
    };
    if (thamso[3] == 'false') {
      gheDuocThem.TrangThai = false;
    } else if (thamso[3] == 'true') {
      gheDuocThem.TrangThai = true;
    }
    console.log(gheDuocThem);
    // sau khi lấy dc input thì : thông qua viewchild, từ comp này chọc vào hàm themghe của con, truyền ghedcthem vào làm tham số
    this.DSGCom.ThemGhe(gheDuocThem);
    //31: sửa h2:/hết project
    this.titleHeading.nativeElement.innerHTML="đã thêm"
  }
  ngOnInit(): void {}
}
